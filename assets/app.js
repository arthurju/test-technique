/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// start the Stimulus application
import './bootstrap';

$(document).ready(function(){
    $(".delete-elmt").on('click', function() {
        var elmt = $(this);
        if (confirm("Voulez-vous supprimer ce jour ?")) {
            $.ajax({
                url: $("#list").data('path'),
                type: 'POST',
                dataType: "json",
                data: {id: $(this).data('id')},
                complete: function (res, statut) {
                    var reponse = res.responseJSON;
                    if (reponse == "OK") {
                        $(elmt).parents('.card').remove();
                    }
                    else {
                        alert('Erreur dans la suppression');
                    }
                }
            });
        }
    })


    // Petite fonction boostrap qui marche pas et j'ai pas le temps de comprendre pourquoi le component close n'est pas inclus :(
    $(document).on('click', '.close', function() {
        $(this).parent().remove();
    });
})
