<?php

namespace App\Controller;

use App\Entity\DayOff;
use App\Form\DayOffType;
use App\Service\WebService\Government\DayOffService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DayOffController extends AbstractController
{
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="list", methods={"GET", "POST"})
     */
    public function list(Request $request) : object
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(DayOffType::class);
        $form->handleRequest($request);

        // Ajout d'un jour férié
        if ($form->isSubmitted() && $form->isValid()) {
            $dayOff = $form->getData();
            $em->persist($dayOff);
            $em->flush();
            $this->addFlash('success', 'Ajout avec succès');
        }

        $jours = $this->getDoctrine()->getRepository(DayOff::class)->findBy([], ['date' => 'DESC']);

        return $this->render('DayOff/list.html.twig', [
            'jours' => $jours,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     *
     * @Route("/fetch", name="fetch", methods={"GET"})
     */
    public function fetch(DayOffService $dayOffService) : object
    {
        // On défini la zone que nous voulons récupérer (ici la métropole)
        $zone = 'metropole';
    
        //On récupère la liste des jours fériés sur les trois dernières années
        $arrayDaysOff = $dayOffService->getLastThreeYearsDaysOff($zone);
    
        $msg = '';
        if (!empty($arrayDaysOff)) {
            // On stock en base les jours que nous ne possédons pas en BDD
            $nbStored = $dayOffService->storeDaysOff($arrayDaysOff);
            $msg = "Aucun nouveau jour férié à ajouter en BDD.";
            if ($nbStored > 0) {
                $msg = $nbStored . " date(s) ajoutée(s) dans la BDD des jours fériés.";
            }
        }
        else {
            $msg = 'Erreur, veuillez vérifier les logs';
        }
        
        return $this->render('DayOff/fetch.html.twig', [
            'msg' => $msg
        ]);
    }
    
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @Route("/delete", name="delete", methods={"POST"})
     */
    public function delete(Request $request) : JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('id');
        $dayOff = $em->getRepository(DayOff::class)->findOneById($id);
        if (!is_null($dayOff)) {
            $em->remove($dayOff);
            $em->flush();

            return new JsonResponse('OK', 200);
        }

        return new JsonResponse('KO', 400);
    }
    
    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \App\Entity\DayOff                        $dayOff
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Route("/update/{id}", name="update", methods={"GET", "POST"})
     */
    public function update(Request $request, DayOff $dayOff) : object
    {
        $form = $this->createForm(DayOffType::class, $dayOff);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Modification du jour férié effectuée.');

            return $this->redirectToRoute('list');
        }

        return $this->render('DayOff/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
