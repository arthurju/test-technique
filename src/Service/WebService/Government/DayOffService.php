<?php

namespace App\Service\WebService\Government;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Entity\DayOff;

class DayOffService
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
    * @param \App\Service\WebService\Gouvernement\ParameterBagInterface $parameterBag
    */
    public function __construct(ParameterBagInterface $parameterBag, LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->host = $parameterBag->get('webservice')['government']['dayoff']['host'];
    }
    
    /**
     * @param string $zone
     *
     * @return array
     */
    public function getLastThreeYearsDaysOff(string $zone) : array
    {
        // Définition de l'année en cours
        $dateObj = new \DateTime();
        $year = $dateObj->format('Y');
        $yearMinusThree = $year - 3;
    
        $arrayDaysOff = [];
        while ($year > $yearMinusThree) {
            $wsReturn = $this->getByZoneAndYear($zone, $year);
            if (!is_null($wsReturn)) {
                $arrayDaysOff[$year] = $wsReturn;
            }
            $year = $year - 1;
        }
        
        return $arrayDaysOff;
    }
    
    /**
     * @param string $zone
     * @param int    $year
     *
     * @return object|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    public function getByZoneAndYear(string $zone, int $year) : ?object
    {
        $client = HttpClient::createForBaseUri($this->host);
        try {
            $return = $client->request(Request::METHOD_GET, $zone . '/' . $year .  '.json');
            if ($return->getStatusCode() != 200) {
                $this->logger->error("Erreur dans le webservice. Mauvaise définition dans la zone ou de l'année");
                return null;
            } else {
                return json_decode($return->getContent());
            }
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e);
        }
        return null;
    }
    
    /**
     * @param array $arrayDaysOff
     *
     * @return int
     */
    public function storeDaysOff(array $arrayDaysOff) : int
    {
        $cleanTableOfDaysOff = $this->checkDaysOff($arrayDaysOff);
        $i = 0;
        foreach ($cleanTableOfDaysOff as $date => $name) {
            $dayOff = new DayOff();
            $formatDateTime = \DateTime::createFromFormat('Y-m-d', $date);
            $dayOff
                ->setName($name)
                ->setDate($formatDateTime)
                ->setManual(0);
            $this->em->persist($dayOff);
            $i++;
        }
        $this->em->flush();
        return $i;
    }
    
    /**
     * @param array $arrayDaysOff
     *
     * @return array
     */
    private function checkDaysOff(array $arrayDaysOff) : array
    {
        $dbDaysOff = $this->em->getRepository(DayOff::class)->findAllWsDaysOff();
        
        $cleanTableOfDaysOff = [];
        
        foreach ($arrayDaysOff as $year => $daysOff) {
            foreach ($daysOff as $date => $dayOffName) {
                if (!in_array($date, $dbDaysOff)) {
                    $cleanTableOfDaysOff[$date] = $dayOffName;
                }
            }
        }
        
        return $cleanTableOfDaysOff;
    }
}
