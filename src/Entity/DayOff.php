<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\DayOffRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DayOffRepository::class)
 * @ORM\Table(name="day_off")
 * @UniqueEntity(fields={"date"}, message="Cette date existe déjà dans la BDD")
 */
class DayOff
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=false, unique=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="Veuillez renseigner un nom")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $manual;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getManual(): ?bool
    {
        return $this->manual;
    }

    public function setManual(bool $manual): self
    {
        $this->manual = $manual;

        return $this;
    }
}
