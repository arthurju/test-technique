<?php

namespace App\Command;

use App\Service\WebService\Government\DayOffService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class WSDayOffCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:ws:dayoff';
    
    /**
     * @var \App\Service\WebService\Government\DayOffService
     */
    private $dayoffService;
    
    /**
     * @var string s
     */
    private $defaultZone = 'metropole';
    
    /**
     * @param \App\Service\WebService\Government\DayOffService $dayoffService
     */
    public function __construct(DayOffService $dayoffService)
    {
        parent::__construct();
        
        $this->dayoffService = $dayoffService;
    }

    protected function configure()
    {
        $this
            ->setDescription('Commande allant chercher les jours fériés sur les 3 dernières années en métropole
            - Define the zone : --zone metropole'
            )
            ->addOption('zone', null, InputOption::VALUE_OPTIONAL);
    }
    
    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        
        $zone = $input->getOption('zone');
        if (empty($zone)) {
            $zone = $this->defaultZone;
        }
        
        //On récupère la liste des jours fériés sur les trois dernières années
        $arrayDaysOff = $this->dayoffService->getLastThreeYearsDaysOff($zone);
        
        if (!empty($arrayDaysOff)) {
            // On stock en base les jours que nous ne possédons pas en BDD
            $nbStored = $this->dayoffService->storeDaysOff($arrayDaysOff);
            $msg = "Aucun nouveau jour férié à ajouter en BDD.";
            if ($nbStored > 0) {
                $msg = $nbStored . " date(s) ajoutée(s) dans la BDD des jours fériés.";
            }
            $io->success($msg);
        }
        else {
            $io->error("Erreur dans l'execution du WS. Veuillez vérifier les logs");
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
