<?php

namespace App\Repository;

use App\Entity\DayOff;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DayOff|null find($id, $lockMode = null, $lockVersion = null)
 * @method DayOff|null findOneBy(array $criteria, array $orderBy = null)
 * @method DayOff[]    findAll()
 * @method DayOff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DayOffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DayOff::class);
    }
    
    /**
     * @return array
     */
    public function findAllWsDaysOff()
    {
        $qb = $this->createQueryBuilder('d')
            ->select('d.date')
            ->andWhere('d.manual = :manual')
            ->setParameter('manual', 0)
            ->getQuery()
            ->getScalarResult();
        
        return array_map('current', $qb);
    }
    
}
