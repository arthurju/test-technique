# Test technique Symfony 5 - Iwit-Systems

### Tâche

Le sujet est simple et se compose de deux tâches principales :

-  1/ Vous devez créer une commande Symfony permettant de récupérer la liste des jours fériés de la métropole pour les 3 dernières années. Les données sont à stocker dans une table
day_off, l'entité DayOff sera à créer.

Les données sont à récupérer sur l'API du gouvernement (https://api.gouv.fr/documentation/jours-feries)

- 2/ Vous devrez créer un CRUD complet permettant de gérer votre entité DayOff.

### Règles

* Le temps est libre mais il est tout de même conseillé de passer moins de 4h sur le sujet (temps de setup d'environnement compris)
* Il est aussi conseillé de faire un maximum de commit pour bien détailler les étapes de votre raisonnement au cours du développement.
* N'hésitez pas à nous faire des retours et nous expliquer les éventuelles problématiques bloquantes que vous auriez rencontrées durant le développement vous empéchant de finir.

### Setup

* La charte graphique n'est pas imposée et sera jugée en bonus. L'emploi d'un framework CSS type Twitter Bootstrap est fortement conseillé.
* L'utilisation de LESS et jQuery est conseillée
* Vous aurez besoin d'un environnement php 7.4, Symfony 5 et un serveur pour l'application.

### Les pré-requis

* Vous êtes libre d'installer des librairies qui peuvent vous aider dans votre développement
* Le choix du client HTTP est laissé à discrétion pour appeller l'API des jours fériés.
* Vous devez respecter les standards d'usage de Symfony ainsi que les standards PHP (PSR)

### Bonus

* L'utilisation d'AJAX pour le CRUD serait appréciée.
* L'implémentation de test unitaires / fonctionnels est un plus.
* Toutes les fonctionnalités que vous aurez le temps d'ajouter seront aussi bonnes à prendre. Un bonus autour de votre créativité pourra être considéré.

### Délivrabilité

* Forkez le projet sur Gitlab et codez directement dans le projet forké.
* Commitez aussi souvent que possible et commentez vos commits pour détailler votre chemin de pensée.
* Mettez à jour le README pour ajouter le temps passé et tout ce que vous jugerez nécessaire de nous faire savoir.
* Envoyez le lien avec le projet à nicolas@iwit-systems.fr.

**Bonne chance !**